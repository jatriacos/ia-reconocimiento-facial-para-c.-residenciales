import cv2
import os


def Prediccion_rostro(dataPath=os.getcwd()):
	#dataPath = 'C:/Users/danie/Documents/RepositoriosMinTic/ReconocimientoFacial/ImagenRostros' #Direccion de las imágenes
	#personName = 'Javier'
	dataPath=os.path.join(dataPath,"ImgRostros")
	imagePaths = os.listdir(dataPath)
	print('imagePaths=',imagePaths)
	cant_fotos=20
	#face_recognizer = cv2.face.EigenFaceRecognizer_create()
	#face_recognizer = cv2.face.FisherFaceRecognizer_create()
	face_recognizer = cv2.face.LBPHFaceRecognizer_create()

	# Leyendo el modelo
	#face_recognizer.read('modeloEigenFace.xml')
	#face_recognizer.read('modeloFisherFace.xml')
	face_recognizer.read('modeloLBPHFace.xml')

	cap = cv2.VideoCapture(0,cv2.CAP_DSHOW)
	#cap = cv2.VideoCapture('Javier.mp4')

	faceClassif = cv2.CascadeClassifier(cv2.data.haarcascades+'haarcascade_frontalface_default.xml')
	num_temp=0
	for i in range(cant_fotos):
		ret,frame = cap.read()
		if ret == False: break
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		auxFrame = gray.copy()

		faces = faceClassif.detectMultiScale(gray,1.3,5)
		
		for (x,y,w,h) in faces:
			rostro = auxFrame[y:y+h,x:x+w]
			rostro = cv2.resize(rostro,(150,150),interpolation= cv2.INTER_CUBIC)
			result = face_recognizer.predict(rostro)
			#print(result)
			if result[1] < 70:
				num_temp+=1
				#print(imagePaths[result[1]])
			
	cap.release()
	cv2.destroyAllWindows()
	print("Num Javiers:",num_temp)
	return num_temp/cant_fotos