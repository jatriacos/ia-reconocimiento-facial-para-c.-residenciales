import psycopg2

class Conexion_db:
    def __init__(self, mi_host,mi_database,mi_user,mi_passwd):
        try:
            self.conn = psycopg2.connect(
                host=mi_host,
                database=mi_database,
                user=mi_user,
                password=mi_passwd)

            # create a cursor
            self.cur = self.conn.cursor()
        except:
            print("conexión no lograda")

    def consultar_db(self,query):   #Método usado para ejecutar queries que retornan
        try:                        #información    
            self.cur.execute(query)
            response = self.cur.fetchall() #trae lo que devuelve la query 
            return response
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)    
            return "error"

    def escribir_db(self,query):   #Método  usado para queries que no retornan
        try:                       #datos: INSERT, CREATE, DELETE 
            self.cur.execute(query)
            self.conn.commit() #con commit se actualizan las tablas
        except (Exception, psycopg2.DatabaseError) as error:
            print("Aqui hay un error: ",error)    
            return "error"
    
    def cerrar_db(self):    #Cierra la conexión con la base de datos
        self.cur.close()
        self.conn.close()
