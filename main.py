from flask import Flask,request,make_response,redirect,render_template
from flask.helpers import make_response
from flask.scaffold import _matching_loader_thinks_module_is_package
from flask.templating import render_template
from werkzeug.wrappers import response
app = Flask(__name__)
from crud import Crud
import json

from CapturandoRostros import CapRostroEntrenamiento
from EntrenandoReconocimientoFacial import Entrenamiento
from ReconocimientoFacial import Prediccion_rostro

my_crud = Crud("ec2-44-196-8-220.compute-1.amazonaws.com","dak1cue8kcvas6","ixpqozqmviaopv","f851242173a35b5f3a4cec6f0b81d28d3572f6a11786e4bd8817d3a6c20667bf")

@app.route('/')
def abrir_login(): #antes leer personaje
    #return render_template("login.html",acceso_cam="none",acceso="none") #Cambie lo de abajo por el return de ahora
    return render_template("/login_sin_imagen.html")

@app.route("/reconocimiento")#Se llama directamente desde el JS del html login.html
def test_javier():
    porcen=Prediccion_rostro()
    if porcen<0.30:
        """ return "NO esta reconocido" #ya no va a login sin imagen """
        #return render_template("login_sin_imagen.html",acceso_cam="block")
        return render_template("semaforo.html",reconoce="1")
    else:
        #crear un semáforo que diga bien pueda, siga
        return render_template("semaforo.html",reconoce="2")
        #return make_response(redirect("/menu"))


""" @app.route("/semaforo", methods=["GET","POST"])
def semaforo():
    if (request.method == "POST"): """

@app.route("/login_sin_imagen", methods=["GET","POST"])
def login():
    nombre_usuario="admin"
    password="123"
    if (request.method=="GET"):
        return render_template("login.html",acceso_cam="none",acceso="none")
    if (request.method=="POST"):
        username=request.form.get("username")
        passwd=request.form.get("psswd")
        if (username==nombre_usuario and passwd==password):
            print("LISTO")
            print(username)
            response = make_response(redirect("/menu"))
            return response 
        else:
            return render_template("login_sin_imagen.html",acceso="block",acceso_cam="none")


@app.route("/menu",methods=["GET","POST"])
def menu():
    if (request.method=="GET"):
        return render_template("/menu.html")
    if (request.method=="POST"):
        selector=request.form.get("seleccion")
        
        if (selector=="registro"):
            print("ESto trae el selector: ",selector)
            response = make_response(redirect('/crear_registro'))
            return response
            #return render_template("/crear_registro")#Por que no puedo utilzar render_template
        if (selector=="actualizar"):
            response =make_response(redirect('/actualizar'))
            return response
        if (selector=="eliminar"):
            response =make_response(redirect('/eliminar'))
            return response
        if (selector=="identificar"): #Cambie vivienda por identificar
            response = make_response(redirect('/identificar'))
            return response    
        """ if (selector=="vivienda"):
            response =make_response(redirect('/ingreso_vivienda'))
            return response  """    
        if (selector=="prueba"):
            response =make_response(redirect('/prueba'))
            return response       
        
@app.route('/crear_registro',methods=["GET","POST"])
def crear_registro():
    #my_crud = Crud("ec2-44-196-8-220.compute-1.amazonaws.com","dak1cue8kcvas6","ixpqozqmviaopv","f851242173a35b5f3a4cec6f0b81d28d3572f6a11786e4bd8817d3a6c20667bf")
    if (request.method=="GET"):
        residencias=my_crud.leer_residencias()
        #print("ESTA ES LA RESIDENCIA INIDCADA. ",residencias)
        return render_template("/Registro_Usuario.html", residencias=residencias) #Para quitar pruebita
    if (request.method=="POST"):
       #id_reside = request.form.get("IdResidencia")
        nombre = request.form.get("nombres_gui")
        apellidos = request.form.get("apellidos_gui")
        tipoDoc = request.form.get("tipoDoc_gui")
        numero = request.form.get("numDoc_gui")
        status = request.form.get("status_gui")
        residencia = request.form.get("residencias")
        print(nombre," ",apellidos," ",tipoDoc," ",numero," ",status," ",residencia)
        
        my_crud.insertar_persona(nombre, apellidos,tipoDoc,numero,status,residencia)              
        response = make_response(redirect('/menu'))
        return response # redict /capturandoNuevo?cc=numero
        #my_crud.insertar_persona('Juana', 'De Arco','CC', '12445623', 'Visiante','NULL')              
    #return render_template("registro_usuario.html")

@app.route('/actualizar') #aqui se muestra la tabla con todos los usuarios del conjunto
                        #ordenados por el número de apto, la idea es que la busqueda luego se
                        #haga por diferentes métodos: por apto, por status, por nombre o por apellido
def actualizar():
    if (request.method=="GET"):
        person = my_crud.persona_rud()
        print(person)
        Listado=[]
        for registros in person:
            Listado.append({"IdResidencia":registros[0],"nombres":registros[1],
            "apellidos":registros[2],"status":registros[3],"tipoDoc":registros[4],"numero":registros[5],
            "IdPersona":registros[6]})
        print(Listado)
        return render_template("listado_general.html",Usuarios=Listado,editar=False)

#en esta función se actualiza la información de página "EDICIÓN DE PERSONAS"
@app.route('/update_registro', methods=["GET","POST"])
def update_registro():
   
    if(request.method=="POST"):
        id = request.form.get("IdPersona")
        nombre = request.form.get("nombres_gui")
        apellidos = request.form.get("apellidos_gui")
        tipoDoc = request.form.get("tipoDoc_gui")
        numero = request.form.get("numDoc_gui")
        status = request.form.get("status_gui")
        residencia = request.form.get("residencias_gui")

        nombreNvo = request.form.get("nombresNvo")
        if not nombreNvo : nombreNvo=nombre
        apellidoNvo = request.form.get("apellidosNvo")   
        if not apellidoNvo : apellidoNvo=apellidos
        tipoDocNvo = request.form.get("tipoDoc_Nvo")
        #Aqui debe salir un letrero que diga algo que el tipo de Doc va a ser cambiado o algo así.
        #if tipoDocNvo != tipoDoc:
        numNvo = request.form.get("num_Nvo")
        if not numNvo : numNvo=numero
        statusNvo = request.form.get("status_Nvo")
        residenciaNva = request.form.get("residencias")
        #Lo mismo con status y con Residencia
        print(nombreNvo,apellidoNvo,tipoDocNvo,numNvo,statusNvo,residenciaNva)
        my_crud.actualizar_persona_existente(id, nombreNvo, apellidoNvo, tipoDocNvo, numNvo,statusNvo, residenciaNva)
        #print("El ID solicitado: ",id)
        #persona=id
        #residencias=my_crud.leer_residencias()
        response = make_response(redirect('/actualizar'))
    return response#make_response(redirect("/menu"))
    #return render_template("editar_usuario.html",persona=persona, residencias=residencias)

#en esta función se abre la hoja de Edición de la persona que está en la tabla
@app.route('/read_person_by_Id', methods=["POST"])
def read_person_by_Id():
    if(request.method=="POST"):
        IdPersona=request.form.get("Boton_editar")
        residencias=my_crud.leer_residencias()
        print("ESTA ES LA RESIDENCIA INIDICADA desde POST: ",residencias)
        print("=================")
        print(IdPersona)
        print("=================")
        persona=my_crud.buscar_por_Id(IdPersona)
        
        print("ESTE ES EL ID CONSEGUIDO:", IdPersona )
    return  render_template("editar_usuario.html",persona=persona, residencias=residencias)   

@app.route("/pasarNvoDto", methods=["GET","POST"]) #Esta en construcción, estoy viendo como actualizo los datos de un campo para pasarlos a otro
def pasarNvoDto():
    if(request.method=="POST"):
        NvoTipoDOc = request.form.get("tipoDoc_guiSelect")
        print("Este es el nuevo Documento:", NvoTipoDOc)
    if(request.method=="GET"):
        print("VAMOS TU PUEDES")


@app.route('/delete_person_by_Id', methods=["POST","GET"])
def eliminar():
    if(request.method=="POST"): #Recojo la información de la página y hago el CRUD
        IdPerson=request.form.get("Boton_eliminar")#tomo el valor del IdPersona
        print("El elemento escogido es:",IdPerson)
        my_crud.eliminar_persona(IdPerson)#Lo paso al método que elimina
        response =make_response(redirect('/actualizar'))
    if (request.method=="GET"): #Hasta el momento he visto que en ningún momento ingresa a este método 
        print("Se ha elimando el personaje ",IdPerson) #es decir que esta línea no se está ejecutando
    return response

    """ return render_template("listado_general.html")#refresca la página """

@app.route('/identificar')#Aqui creo el método que me identifica la persona que está al frente de cámara
def identificando():
    return render_template("login.html",acceso_cam="none",acceso="none")

@app.route('/ingreso_vivienda')
def vivienda():
    return render_template("residencia.html")



    """my_crud = Crud("ec2-44-196-8-220.compute-1.amazonaws.com","dak1cue8kcvas6","ixpqozqmviaopv","f851242173a35b5f3a4cec6f0b81d28d3572f6a11786e4bd8817d3a6c20667bf")
    personas = my_crud.leer_persona()
    personaje = personas[3]
    print (personaje)
    retorno = json.dumps({"id":personaje[0],"nombres":personaje[1],
    "apellidos":personaje[2]})
 
    return retorno"""

""" @app.route('/busqueda') #Encuentra a la primera persona según el apellido y lo muestra por pantalla
def encontrar_personaje():
    my_crud = Crud("ec2-44-196-8-220.compute-1.amazonaws.com","dak1cue8kcvas6","ixpqozqmviaopv","f851242173a35b5f3a4cec6f0b81d28d3572f6a11786e4bd8817d3a6c20667bf")
    personas = my_crud.leer_persona()

    lista = []
    for registro in personas: 
        lista.append({"id":registro[0], "nombres":registro[1],
        "apellidos":registro[2],"tipo_doc":registro[3],"numero":registro[4],
        "status":registro[5],"residencia":registro[6]})
    
    print(lista)

    valor = "Villada"
    for i in range(len(lista)):
        if (lista[i].get("apellidos")==valor):
            print("la lista encontrada",lista[i])
            registro={"Id":str(lista[i].get("id")),"Nombres": lista[i].get("nombres"),
            "Apellidos": lista[i].get("apellidos"),"Tipo de Documento":lista[i].get("tipo_doc"),
            "Número":lista[i].get("numero"),"Status":lista[i].get("status"),
            "Residencia":lista[i].get("residencia")}
            
            registro = json.dumps(registro)
            return registro """


@app.route('/listado')
def listado_general():
    render_template("listado_general.html")

#PRUEBAS DIRECTAS SIN PASAR POR EL LOCALHOST
#my_crud = Crud("localhost","IA_Residencial","postgres","qwerty")
#my_crud = Crud("ec2-44-196-8-220.compute-1.amazonaws.com","dak1cue8kcvas6","ixpqozqmviaopv","f851242173a35b5f3a4cec6f0b81d28d3572f6a11786e4bd8817d3a6c20667bf")

#my_crud.insertar_persona('Juana', 'De Arco','CC', '12445623', 'Visiante','NULL') 
#my_crud.insertar_persona('Laura 3', 'Perry','CE', '12457896', 'Domiciliario','NULL') 
#my_crud.insertar_residencia('Torre 1 Apto 203') 

#my_crud.leer_persona()
#my_crud.actualizar_persona_existente('1','Torre 1 Apto 104')

#my_crud.eliminar_persona('6')
#===================================================

@app.route("/capturandoNuevo")
def capturandoNuevo():
    if request.method=="GET":
        personID = request.args.get("id",default=1,type=int)
        #CapRostroEntrenamiento("personName=personID")
        CapRostroEntrenamiento("JAVIER-4")
        
        return redirect("/entrenamiento")#redirect("/entrenamiento") #personID
  
@app.route("/entrenamiento")
def test_entrenamiento():
    porcen=Entrenamiento()
    return "Entrenado"

@app.route("/prueba")
def prueba():
    if(request.method=="GET"):
        
        residencias=my_crud.leer_residencias()
        IdPerso=request.form.get("Boton_otro")
        print("ESTA ES LA RESIDENCIA INIDCADA. ",residencias)
        print("Este es ID que trajo:",IdPerso)
        return render_template("/pruebas.html", residencias=residencias)
    if(request.method=="POST"):
        IdPersona=request.form.get("Boton_otro")
        """ persona=my_crud.buscar_por_Id(IdPersona) """
        return render_template("/pruebas.html", xxx=IdPersona) 

if __name__=="__main__": #punto de partida, donde python empieza a ejecutar
	while(True):
		print("starting web server")
		app.run(debug=True,host='0.0.0.0')