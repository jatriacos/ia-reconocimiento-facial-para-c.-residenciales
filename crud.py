from Conexion_db import Conexion_db


class Crud:
    def __init__(self, host, db, user, passwd):
        self.my_conn = Conexion_db(host, db, user, passwd)

    #Inserta los datos de una persona nueva
    def insertar_persona(self, nombres, apellidos, tipoDoc, numero, status, IdResidencia):
        query ="INSERT INTO \"Persona\" ("+\
        "nombres, apellidos,  \"tipoDoc\", numero, status, \"IdResidencia\") "+\
               "VALUES ('"+nombres+ "','"+apellidos+ "','"+tipoDoc+ "',"+numero+ ",'"\
                    +status+"','"+IdResidencia+"')"
        print(query)
        self.my_conn.escribir_db(query)

    #Inserta el numero de unidades residenciales del Conjunto
    def insertar_residencia(self, IdResidencia):
        query = "INSERT INTO \"Residencia\"(\"IdResidencia\") VALUES('"+IdResidencia+"')"
        self.my_conn.escribir_db(query)

    #Actualiza información del apto según el IdPersona
    def actualizar_aptoXpersona_existente(self, IdPersona, IdResidencia):    
        query = """UPDATE "Persona" SET "IdResidencia"='{IdResidencia}'
        WHERE "IdPersona" = {IdPersona}""".format(IdResidencia= IdResidencia, IdPersona= IdPersona)
        print(query)  
        self.my_conn.escribir_db(query)

    #Elimina el registro de una persona según el IdPersona
    def eliminar_persona(self, IdPersona):
        query = """DELETE FROM "Persona"
        WHERE "IdPersona" = {IdPersona}""".format(IdPersona= IdPersona)
        print(query)  
        self.my_conn.escribir_db(query)

    def leer_persona(self):
        query =  "SELECT \"IdPersona\", nombres, apellidos, \"tipoDoc\","+\
            " numero, status, \"IdResidencia\" FROM \"Persona\" "
        print(query)
        respuesta = self.my_conn.consultar_db(query)
        return respuesta
    #Actualiza toda la información correspondiente a una persona desde la hoja Edición de Personas
    def actualizar_persona_existente(self, IdPersona, nombre, apellidos, tipoDoc, numero,status, residencia):
        query = "UPDATE \"Persona\" SET nombres='"+nombre+"',apellidos='"+apellidos+"',"+\
            "\"tipoDoc\"='"+tipoDoc+"', numero='"+numero+"', status='"+status+"',\"IdResidencia\"='"+residencia+"'"+\
            "WHERE \"IdPersona\"='"+IdPersona+"'"
        self.my_conn.escribir_db(query)   

    def persona_rud(self):
        query = "SELECT \"IdResidencia\", nombres, apellidos, status, \"tipoDoc\", "+\
            "numero, \"IdPersona\" FROM \"Persona\" ORDER BY \"IdResidencia\", nombres"
        
        print(query)    
        respuesta = self.my_conn.consultar_db(query)
        return respuesta

    def leer_residencias(self):
        query = "SELECT \"IdResidencia\" FROM \"Residencia\" "
        print (query)
        respuesta = self.my_conn.consultar_db(query)
        viviendas=[]
        for vivienda in respuesta:
            viviendas.append(vivienda[0])
        print (viviendas)

        return viviendas

    def buscar_por_Id(self, IdPersona):
        query = "SELECT \"IdPersona\", nombres, apellidos, \"tipoDoc\","+\
            " numero, status, \"IdResidencia\" FROM \"Persona\"" +\
            " WHERE\"IdPersona\"="+str(IdPersona) # print(2) ---> 2 print("2") ---> 2
        respuesta = list(self.my_conn.consultar_db(query)[0])
        persona = {"IdPersona":respuesta[0],"nombres":respuesta[1],"apellidos":respuesta[2],
        "tipoDoc":respuesta[3],"numero":respuesta[4],"status":respuesta[5],"Residencia":respuesta[6]}
        '''
        persona=[]
        for person_choice in respuesta:
            persona.append({"IdResidencia":person_choice[0],"nombres":person_choice[1]})
        print(persona)  
        '''  
        return persona

